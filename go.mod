module codeberg.org/kmpm/goglass

go 1.19

require (
	github.com/go-chi/chi/v5 v5.0.7 // indirect
	github.com/santhosh-tekuri/jsonschema/v3 v3.1.0 // indirect
	github.com/swaggest/form/v5 v5.0.1 // indirect
	github.com/swaggest/jsonschema-go v0.3.35 // indirect
	github.com/swaggest/openapi-go v0.2.18 // indirect
	github.com/swaggest/refl v1.1.0 // indirect
	github.com/swaggest/rest v0.2.30 // indirect
	github.com/swaggest/swgui v1.5.1 // indirect
	github.com/swaggest/usecase v1.1.3 // indirect
	github.com/vearutop/statigz v1.1.5 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
)
