package version1

import (
	"context"

	"github.com/go-chi/chi/v5/middleware"
	"github.com/swaggest/rest/nethttp"
	"github.com/swaggest/rest/web"
	"github.com/swaggest/usecase"
)

func world() usecase.Interactor {
	type input struct {
		Name string `query:"name"`
	}

	u := usecase.NewInteractor(func(ctx context.Context, in input, out *string) error {
		*out = in.Name
		return nil
	})
	return u
}

func New(parent *web.Service) *web.Service {
	s := web.DefaultService()

	s.Wrap(
		middleware.BasicAuth("Admin Access", map[string]string{"admin": "admin"}),
		nethttp.HTTPBasicSecurityMiddleware(parent.OpenAPICollector, "Admin", "Admin access"),
	)
	s.Get("/hello", world())

	return s
}
