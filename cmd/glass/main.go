package main

import (
	"log"
	"net/http"

	"codeberg.org/kmpm/goglass/api/version1"
	"github.com/swaggest/rest/web"
	"github.com/swaggest/swgui/v4emb"
)

func main() {
	s := web.DefaultService()
	s.OpenAPI.Info.Title = "Go Glass"
	s.OpenAPI.Info.Version = "v0.0.1"
	s.Mount("/api/v1", version1.New(s))
	s.Docs("/api/v1/docs", v4emb.New)

	// Blanket handler, for example to serve static content.
	s.Mount("/", http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_, _ = w.Write([]byte("blanket handler got a request: " + r.URL.String()))
	}))

	log.Println("http://localhost:8011/api/v1/docs")
	if err := http.ListenAndServe(":8011", s); err != nil {
		log.Fatal(err)
	}
}
